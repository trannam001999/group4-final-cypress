const { defineConfig } = require("cypress");
const {downloadFile} = require('cypress-downloadfile/lib/addPlugin');
const { isFileExist, findFiles } = require('cy-verify-downloads');
const fs = require('fs');

module.exports = defineConfig({
  defaultCommandTimeout: 3000,
  env: {
    baseApiUrl: 'https://api.unsplash.com',
    // userEmail: 'tokwwnhi@gmail.com',
    // userPassword: 'Kieunhi1412',
    // userName: "tokieunhi",
    // token: "hzVdVH1Zjax-dJ_rYfLIMEg3Pzps2yzjT3fqF5v3340",
    
    userEmail: 'trannam001999@gmail.com',
    userPassword: 'namtran@123',
    userName: "namtran123",  
    token:"6g_PWdlZCGp6maMWycPrx6OcydqXeBYjhIyyarlGW2s",
  },
  // retries: {
  //   runMode: 1,
  //   openMode: 1
  // },
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      on('task', { isFileExist, findFiles });
      on('task', {downloadFile});
      on('task', {
        downloads:  (downloadspath) => {
          return fs.readdirSync(downloadspath)
        }
      })
    },
    baseUrl: 'https://unsplash.com',
    trashAssetsBeforeRuns: true
  },
});
