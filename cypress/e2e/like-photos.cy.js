const { UrlConstants } = require('../constants/url-constants')
const { LikesPage } = require('../pages/likes-page')
const { ApiConstants } = require('../constants/api-constants')


describe('Like photos spec', function () {
  
  let arrayLinkDownloadPhotoInHomePage = [];
  let randomPhotoId = [];
  let username;

  beforeEach(function () {
    // cy.fixture('test-account.json').as('testAccount');
    cy.createRequest('GET', ApiConstants.USER_PROFILE_ENDPOINT).then((response) => {
      username = response.body.username
      cy.log(username)
      cy.visit(UrlConstants.LOGIN_URL)
      cy.login(Cypress.env('userEmail'), Cypress.env('userPassword'))
      //cy.login(this.testAccount.KieuNhi.userEmail, this.testAccount.KieuNhi.userPassword)
      cy.visit(UrlConstants.LIKES_URL.replace('%s', username))
    })

  })

  it('Like 3 random photos successfully', function () {

    cy.log('Start test')

    for (let i = 0; i < 3; i++) {
      //get random photo
      cy.createRequest('GET', ApiConstants.GET_RANDOM_PHOTO_ENDPOINT)
        .then(({ body }) => {
          randomPhotoId.push(body.id)
          let photoId = body.id
          cy.log(photoId)

          //like photo
          cy.createRequest('POST', ApiConstants.LIKE_PHOTO_ENDPOINT.replace('%s', photoId))
            .then(res => {              
              arrayLinkDownloadPhotoInHomePage.push(res.body.photo.links.download)
            })
          //})
        })
    }

    cy.visit(UrlConstants.LIKES_URL.replace('%s', username))

    //Verify total likes photos
    LikesPage.getActualTotalLikes().should('have.text', '3').then(() => {
      for (let j = 0; j < arrayLinkDownloadPhotoInHomePage.length; j++) {
        cy.log('Assert like photo ' + arrayLinkDownloadPhotoInHomePage[j] + ' >< ' + LikesPage.getLinkDownloadPhotoByOrder(j + 1))

        LikesPage.getLinkDownloadPhotoByOrder(j + 1).should((a)=>{
          expect(a).to.contain(arrayLinkDownloadPhotoInHomePage[arrayLinkDownloadPhotoInHomePage.length - (j+1)])
        })
      }
    })



  })

  afterEach(function () {
    //Unlike 3 random photos liked
    for (let i = 0; i < 3; i++) {
      cy.log(randomPhotoId[i])
      cy.createRequest('DELETE', ApiConstants.LIKE_PHOTO_ENDPOINT.replace('%s', randomPhotoId[i]))
    };
  })
})