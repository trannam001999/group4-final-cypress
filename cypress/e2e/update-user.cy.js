const { UrlConstants } = require('../constants/url-constants')
const { HomePage } = require('../pages/home-page')
const { ProfilePage } = require('../pages/profile-page')
const { UpdateProfilePage } = require('../pages/update-profile-page')

describe('update user info spec', () => {
    beforeEach('login', () => {
        cy.visit(UrlConstants.LOGIN_URL)
        cy.login(Cypress.env('userEmail'), Cypress.env('userPassword'))
        cy.fixture('updated-user-info.json').as('updatedInfo')
    })

    it('edit user', function () {
        HomePage.navigateProfilePage()
        ProfilePage.clickEditProfileBtn()
        UpdateProfilePage.inputFirstName(this.updatedInfo.firstName)
        UpdateProfilePage.inputLastName(this.updatedInfo.lastName)
        UpdateProfilePage.inputEmail(this.updatedInfo.email)
        UpdateProfilePage.inputUserName(this.updatedInfo.userName)
        UpdateProfilePage.inputLocation(this.updatedInfo.location)
        UpdateProfilePage.inputPersonalSite(this.updatedInfo.personalSite)
        UpdateProfilePage.inputBio(this.updatedInfo.bio)
        UpdateProfilePage.inputInstagramUsername(this.updatedInfo.instagramUsername)
        UpdateProfilePage.inputTwitterUsername(this.updatedInfo.twitterUsername)
        UpdateProfilePage.inputPaypalEmail(this.updatedInfo.paypalEmail)
        UpdateProfilePage.clickUpdateAccountBtn()
        cy.visit("/@" + this.updatedInfo.userName)
        var fullName = this.updatedInfo.firstName + " " + this.updatedInfo.lastName
        ProfilePage.getTextFullName().should('have.text', fullName)
    })

    afterEach("empty spec", () => {
        cy.updateDefaultUsername()
    })
})