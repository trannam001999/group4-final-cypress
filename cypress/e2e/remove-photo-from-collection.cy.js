
const { UrlConstants } = require('../constants/url-constants')
const { CollectionPage } = require('../pages/collection-page')
const { ApiConstants } = require('../constants/api-constants');
describe('remove photo from collection spec', () => {
  let randomPhotoId = [];
  let collectionId;
  let username;

  //@BeforeMethod
  beforeEach(() => {
    cy.createRequest('GET', ApiConstants.USER_PROFILE_ENDPOINT).then((response) => {
      username = response.body.username
      cy.log(username)

      let url = ApiConstants.GET_USER_COLLECTIONS.replace('%s', username)
      //call api to get collection id
      cy.createRequest('GET', url, null).then((response) => {
        collectionId = response.body[0].id

        for (let i = 0; i < 2; i++) {
          //call api to get 2 random photos
          cy.createRequest('GET', ApiConstants.GET_RANDOM_PHOTO_ENDPOINT)
            .then(({ body }) => {
              randomPhotoId.push(body.id)
              const payload = {
                photo_id: `${body.id}`
              }
              let url = ApiConstants.ADD_PHOTO_2_COLLECTION_ENDPOINT.replace('%s', collectionId)

              //call api to add 2 random photos to collection
              cy.createRequest('POST', url, payload)
            })
        }
        cy.visit(UrlConstants.LOGIN_URL)
        cy.login(Cypress.env('userEmail'), Cypress.env('userPassword'))
        CollectionPage.goToMyCollection(collectionId)

  
      })
    })

  })

  //@Test
  it('remove photo from collection successfully', () => {
    const payload = {
      photo_id: `${randomPhotoId[1]}`
    }
    let url = ApiConstants.REMOVE_PHOTO_FROM_COLLECTION.replace('%s', collectionId)
    cy.createRequest('DELETE', url, payload).then(() => {
      CollectionPage.goToMyCollection(collectionId)
      CollectionPage.getPhotoCountInCollection().should('have.length', 1)
    })

  })

  afterEach(() => {
    const payload = {
      photo_id: `${randomPhotoId[0]}`
    }
    let url = ApiConstants.REMOVE_PHOTO_FROM_COLLECTION.replace('%s', collectionId)
    cy.createRequest('DELETE', url, payload)
  })
})