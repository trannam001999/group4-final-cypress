const { UrlConstants } = require('../constants/url-constants')
const { HomePage } = require('../pages/home-page')
const { PhotoDetailPage } = require('../pages/photo-detail-page')



describe('follow photographer', function () {
    beforeEach(function () {
        cy.visit(UrlConstants.LOGIN_URL)
        cy.login(Cypress.env('userEmail'), Cypress.env('userPassword'))
        HomePage.clickFirstImg()

    })

    it('follow photographer successfully', function () {

        PhotoDetailPage.hoverOnIconBtn()
        PhotoDetailPage.clickFollowBtn()
        PhotoDetailPage.getColorBtn()
            .should('have.css', 'background-color')
            .and('eq', 'rgb(238, 238, 238)')
        PhotoDetailPage.getColorBtn()
            .should('have.attr', 'title')
            .and('eq', 'Following')


    })

    afterEach(() => {
        PhotoDetailPage.clickFollowBtn()
    })



})