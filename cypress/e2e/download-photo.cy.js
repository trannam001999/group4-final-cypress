const { UrlConstants } = require('../constants/url-constants')
const { ApiConstants } = require('../constants/api-constants');

describe('download photo spec', () => {
    let randomPhotoId;
    let photo_url;
    let username;

    //@BeforeMethod
    beforeEach(() => {
        cy.createRequest('GET', ApiConstants.USER_PROFILE_ENDPOINT).then((response) => {
            username = response.body.username
            cy.log(username)
        })
        cy.createRequest('GET', ApiConstants.GET_RANDOM_PHOTO_ENDPOINT)
            .then(({ body }) => {
                randomPhotoId = body.id
                cy.log(randomPhotoId)
                cy.createRequest('GET', ApiConstants.GET_PHOTO_URL_ENDPOINT.replace('%s', randomPhotoId)).then(url => {
                    photo_url = url.body.url
                    cy.log(photo_url)
                    cy.visit(UrlConstants.LOGIN_URL)
                    cy.login(Cypress.env('userEmail'), Cypress.env('userPassword'))
                    cy.visit(UrlConstants.PHOTO_DETAIL.replace('%s', randomPhotoId))
                })
            })
    })


    //@Test
    it('download photo', () => {
        let fileName = randomPhotoId + '.jpg';
        cy.downloadFile(photo_url, 'cypress/downloads', fileName)
        cy.verifyDownload(fileName, {contains: true})       
    })

})