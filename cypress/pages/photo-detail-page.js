const BTN_FOLLOW = "//a[@href='/@wasacrispbread']/following-sibling::div/button";
const BTN_PHOTOGRAPHER = "//div[@data-test='photos-route']//header//a//img";

export const PhotoDetailPage = {
    hoverOnIconBtn(){
        cy.xpath(BTN_PHOTOGRAPHER).realHover('mouse')
    },

    clickFollowBtn(){
        cy.xpath(BTN_FOLLOW).click()
    },
    getColorBtn(){
        return cy.xpath(BTN_FOLLOW)
    }
}