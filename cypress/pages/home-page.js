// home page
const LNK_LOGIN = "//a[text()='Log in']"
const BTN_DOWNLOAD_BY_ORDER = "(//a[@title='Download photo'])[%s]"
const BTN_LIKE_BY_ORDER = "(//button[@title='Like'])[%s]"
const FIRST_IMG = "(//figure[@itemprop='image'])[1]";

export const HomePage = {

    clickLoginLnk() {
        cy.xpath(LNK_LOGIN).click()
    },

    navigateProfilePage(){
        cy.visit("/@" + Cypress.env('userName'));
    },

    getLinkDownloadPhotoByOrder(randomNumber) {
        return cy.xpath(BTN_DOWNLOAD_BY_ORDER.replace('%s', randomNumber)).invoke('attr', 'href')
    },

    clickLikePhotoButtonByOrder(randomNumber) {
        cy.xpath(BTN_LIKE_BY_ORDER.replace('%s', randomNumber)).click({force: true})
    },

    clickFirstImg(){
        cy.xpath(FIRST_IMG).click()
    }
}