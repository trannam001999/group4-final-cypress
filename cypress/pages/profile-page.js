const BTN_EDIT_PROFILE = "//a[text()='Edit profile']"
const LBL_FULLNAME = "//a[text()='Edit profile']//ancestor::div[4]/div[1]"

export const ProfilePage = {
    clickEditProfileBtn() {
        cy.xpath(BTN_EDIT_PROFILE).click()
    },

    getTextFullName() {
        return cy.xpath(LBL_FULLNAME)
    }
}