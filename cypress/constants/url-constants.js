export const UrlConstants = {
    LOGIN_URL: "/login",
    PHOTO_DETAIL: "/photos/%s",
    LIKES_URL: "/@%s/likes"
 };