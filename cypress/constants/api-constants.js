export const ApiConstants = {
    //end point
    USER_PROFILE_ENDPOINT: "/me",
    ADD_PHOTO_2_COLLECTION_ENDPOINT: "/collections/%s/add",
    GET_RANDOM_PHOTO_ENDPOINT: "/photos/random",
    REMOVE_PHOTO_FROM_COLLECTION: "/collections/%s/remove",
    GET_USER_COLLECTIONS: "/users/%s/collections",
    GET_PHOTO_URL_ENDPOINT: "/photos/%s/download",
    LIKE_PHOTO_ENDPOINT: "/photos/%s/like"
 };